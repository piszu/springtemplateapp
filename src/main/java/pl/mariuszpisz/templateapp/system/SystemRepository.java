package pl.mariuszpisz.templateapp.system;

import org.springframework.data.jpa.repository.JpaRepository;

interface SystemRepository extends JpaRepository<System, String> {
    boolean existsByTag(String tag);
}
