package pl.mariuszpisz.templateapp.system;

import org.mapstruct.Mapper;
import pl.mariuszpisz.templateapp.system.api.SystemDto;

@Mapper(componentModel = "spring")
public interface SystemMapper {
    SystemDto map(System system);

    System map(SystemDto dto);
}
