package pl.mariuszpisz.templateapp.system;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import pl.mariuszpisz.templateapp.common.exceptions.ResourceAlreadyExistException;
import pl.mariuszpisz.templateapp.common.exceptions.ResourceNotFoundException;

import java.util.Collection;

import static java.lang.String.format;

@Slf4j
@Service
@AllArgsConstructor
public class SystemService {

    SystemRepository systemRepository;

    public Collection<System> getAllSystems() {
        log.info("Request to find all systems");

        var systemList = systemRepository.findAll();

        return systemList;
    }

    public System getSystemByTag(String tag) {
        return systemRepository.findById(tag)
                .orElseThrow(() -> new ResourceNotFoundException(
                        format("System with tag %s does not exist", tag)));
    }

    public boolean systemExistByTag(String systemTag) {
        return systemRepository.existsByTag(systemTag);
    }

    public String createSystem(System newSystem) {
        checkDuplicate(newSystem);

        var system = systemRepository.save(newSystem);

        return system.getTag();
    }

    private void checkDuplicate(System system) {
        if (systemRepository.existsByTag(system.getTag())) {
            throw new ResourceAlreadyExistException(format(
                    "System with tag: %s already exist", system.getTag()));
        }
    }
}
