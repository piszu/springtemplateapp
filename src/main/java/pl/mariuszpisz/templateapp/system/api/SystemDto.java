package pl.mariuszpisz.templateapp.system.api;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class SystemDto {
    @NotBlank
    private String tag;

    @NotBlank
    private String name;

    @NotBlank
    private String description;

}