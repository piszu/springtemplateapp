package pl.mariuszpisz.templateapp.system.api;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.mariuszpisz.templateapp.system.SystemMapper;
import pl.mariuszpisz.templateapp.system.SystemService;

import java.net.URI;
import java.util.Collection;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.created;
import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentContextPath;
import static pl.mariuszpisz.templateapp.system.api.SystemController.SYSTEM_URL;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping(path = SYSTEM_URL, produces = APPLICATION_JSON_VALUE)
public class SystemController {
    public final static String SYSTEM_URL = "/api/systems";

    SystemService systemService;
    SystemMapper systemMapper;

    @GetMapping
    ResponseEntity<Collection<SystemDto>> getAllSystems() {
        log.info("Request to get list of all existing systems");

        var systemList = systemService.getAllSystems()
                .stream().map(systemMapper::map)
                .collect(toList());

        return ok().body(systemList);
    }

    @GetMapping(value = "/{systemTag}")
    ResponseEntity<SystemDto> getSystem(@PathVariable String systemTag) {
        var system = systemService.getSystemByTag(systemTag);

        return ok().body(systemMapper.map(system));
    }

    @PostMapping
    ResponseEntity<Collection<SystemDto>> createSystem(@RequestBody SystemDto newSystem) {
        var newSystemTag = systemService.createSystem(systemMapper.map(newSystem));

        return created(resourceLocation(newSystemTag)).build();
    }

    private URI resourceLocation(String id) {
        return fromCurrentContextPath()
                .path(SYSTEM_URL)
                .path("/{id}").buildAndExpand(id).toUri();
    }
}
