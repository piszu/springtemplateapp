package pl.mariuszpisz.templateapp.system;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "SYSTEMS")
public class System {
    @Id
    private String tag;
    private String name;
    private String description;
}

