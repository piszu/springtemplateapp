package pl.mariuszpisz.templateapp.common.exceptions;

public class InvalidInputDataException extends RuntimeException {
    public InvalidInputDataException() {
    }

    public InvalidInputDataException(String message) {
        super(message);
    }
}