package pl.mariuszpisz.templateapp.common.exceptions;

public class UserAuthorizationNotFoundException extends ResourceNotFoundException {
    public UserAuthorizationNotFoundException() {
        super();
    }

    public UserAuthorizationNotFoundException(String message) {
        super(message);
    }
}