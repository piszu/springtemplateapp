package pl.mariuszpisz.templateapp.common.exceptions;

public class InactiveAuthorizationException extends RuntimeException {
    public InactiveAuthorizationException() {
    }

    public InactiveAuthorizationException(String message) {
        super(message);
    }
}