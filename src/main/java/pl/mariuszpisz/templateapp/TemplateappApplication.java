package pl.mariuszpisz.templateapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TemplateappApplication {

	public static void main(String[] args) {
		SpringApplication.run(TemplateappApplication.class, args);
	}

}
